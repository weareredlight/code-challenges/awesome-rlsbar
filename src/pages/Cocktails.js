import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';

import { getRandomDrink } from '../api';

class Cocktails extends React.Component {
  state = {
    loading: false,
    cocktails: [],
    drink: null
  }

	async componentDidMount() {
    this.setState({ loading: true });
    const randDrink = await getRandomDrink();
    this.setState({ loading: false, drink: randDrink });
  }

  render() {
    const { loading, drink } = this.state;
    return (
      <div>
        <Jumbotron>
          <h1 className="header">Cocktails</h1>
        </Jumbotron>
        <Container>
          <Row>
            <Col>
              {loading && (
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              )}
              <pre>
                <code>
                  {drink && JSON.stringify(drink)}
                </code>
              </pre>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Cocktails;
import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

import { saveTodosToStorage, loadTodosFromStorage } from '../utils';

const TODOS_EXAMPLE = [
  "1. Page with a list of the alphabet letters (A-Z). By clicking on a letter, it should search drinks started by this letter.",
  "2. Individual cocktail page",
  "2.1. Ingredients should have a link to an individual page",
  "2.2. Text input to filter the list of ingredients",
  "3. Individual category page that shows a list of cocktails",
  "3.1. It should be possible to sort the list alphabetically"
];

class Todo extends React.PureComponent {
  state = {
    todos: TODOS_EXAMPLE.map(label => ({ label, checked: false })),
  }

  componentDidMount() {
    const storedTodos = loadTodosFromStorage();
    if (storedTodos) {
      this.setState({ todos: storedTodos });
    }
  }

  componentDidUpdate() {
  }

  handleTodoChange = index => (e) => {
    const checked = e.target.checked;
    const todo = this.state.todos[index];

    if (!todo) return;

    let newList = [...this.state.todos];
    newList[index] = { ...todo, checked };
    this.setState({ todos: newList });
    saveTodosToStorage(newList);
  }

  render() {
    return (
      <Row>
        <Col>
          <h1>Todo</h1>
          <div key="default-checkbox" className="mb-5">
            {this.state.todos.map((t, index) => (
              <Form.Check
                key={`t-${index}`}
                type="checkbox"
                id={`t-${index}`}
                label={t.label}
                onChange={this.handleTodoChange(index)}
                checked={t.checked}
                value={t.checked}
              />
            ))}
          </div>
        </Col>
      </Row>
    );
  }
}

export default Todo;